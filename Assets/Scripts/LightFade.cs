﻿using System.Collections;
using UnityEngine;

public class LightFade : MonoBehaviour
{
    public float fadeUntil;

    Light objectLight;
    Coroutine tempCor;

    float tempFloat;
    float lerpValue;
    float time=0.4f;
    
    private void Start() {
        objectLight = GetComponent<Light>();
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.G)) {
            FadeIn();
        }
        if(Input.GetKeyDown(KeyCode.H)) {
            FadeOut();
        }
    }

    public void FadeIn() {
        if(tempCor!=null) {
            StopCoroutine(tempCor);
        }
        tempCor = StartCoroutine(FadeInCor());
    }

    public void FadeOut() {
        if(tempCor!=null) {
            StopCoroutine(tempCor);
        }
        tempCor = StartCoroutine(FadeOutCor());
    }

    IEnumerator FadeInCor() {
        tempFloat = objectLight.intensity;
        lerpValue = 0f;
        while(lerpValue<=time) {
            Debug.Log(objectLight.intensity);
            objectLight.intensity = Mathf.Lerp(tempFloat , fadeUntil,(lerpValue/time));
            // objectLight.intensity = tempFloat;
            lerpValue += Time.deltaTime;
            yield return null;
        }
        objectLight.intensity = fadeUntil;
    }

    IEnumerator FadeOutCor() {
        tempFloat = objectLight.intensity;
        lerpValue = 0f;
        while(lerpValue<=time) {
            objectLight.intensity = Mathf.Lerp(tempFloat , 0,(lerpValue/time));
            // objectLight.intensity = tempFloat;
            lerpValue += Time.deltaTime;
            yield return null;
        }
        objectLight.intensity = 0;
    }
}