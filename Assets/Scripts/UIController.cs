﻿using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public static UIController instance;

    public Crosshair crosshair;
    public FadePanel fadePanel;
    public MainMenu mainMenu;
    
    private void Awake() {
        instance = this;
        DontDestroyOnLoad(this);
        // Cursor.lockState = CursorLockMode.None;
    }

    public List<GameObject> UITree = new List<GameObject>();

    public void Open(GameObject obj) {
        UITree[UITree.Count-1].SetActive(false);
        obj.SetActive(true);
        UITree.Add(obj);
    }

    public void CloseLast() {
        if(UITree.Count > 0) {
            UITree[UITree.Count-1].SetActive(false);
            UITree.RemoveAt(UITree.Count-1);
            if(UITree.Count > 0) {
                UITree[UITree.Count-1].SetActive(true);
            }
            else {
                PlayerMovement.blockMovement = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.Escape)) {
            CloseLast();
        }
    }
}