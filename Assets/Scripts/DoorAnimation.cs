﻿using System.Collections;
using UnityEngine;

public class DoorAnimation : Interactable
{
    public float[] angles;

    int currentState = 0;
    float lerpValue = 0f;
    float time = 0.4f;
    float currentAngleY = 0f;

    Coroutine tempCor;
    Vector3 currentAngle;

    public override void Click() {
        currentState++;
        if(currentState>=angles.Length) {
            currentState = 0;
        }
        if(tempCor!=null) {
            StopCoroutine(tempCor);
        }
        tempCor = StartCoroutine(NextState());
    }

    IEnumerator NextState() {
        lerpValue = 0f;
        currentAngle = transform.localEulerAngles;
        currentAngleY = currentAngle.y;
        while(lerpValue < time) {
            currentAngle.y = Mathf.LerpAngle(currentAngleY, angles[currentState], lerpValue/time);
            transform.localEulerAngles = currentAngle;
            lerpValue += Time.deltaTime;
            yield return null;
        }
        // transform.eulerAngles = 
        //TODO: Set final pisition;
    }
}