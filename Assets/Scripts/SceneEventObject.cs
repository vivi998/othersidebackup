﻿using UnityEngine;

public class SceneEventObject : MonoBehaviour
{
    public void NextScene() {
        SceneController.instance.NextScene();
    }

    public void ReloadScene() {
        SceneController.instance.ReloadScene();
    }

    public void GoToScene(int index) {
        SceneController.instance.GoToScene(index);
    }
}