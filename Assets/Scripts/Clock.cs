using UnityEngine;
using System;

public class Clock : MonoBehaviour {

	private const float
	hoursToDegrees = -360f / 12f,
	minutesToDegrees = -360f / 60f,
	secondsToDegrees = -360f / 60f;

	public Transform hours, minutes, seconds;

	void Update () {
		hours.localRotation = Quaternion.Euler(0f, 0f, DateTime.Now.Hour * -hoursToDegrees);
		minutes.localRotation = Quaternion.Euler(0f, 0f, DateTime.Now.Minute * -minutesToDegrees);
		seconds.localRotation = Quaternion.Euler(0f, 0f, DateTime.Now.Second * -secondsToDegrees);
	}
}