﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FadePanel : MonoBehaviour
{
    public Image fadeImg;
    Coroutine tempCor;

    float lerpValue=0f;
    float time=0.2f;

    Color tempColor;

    private void Start() {
        SetFadePanelAlpha(1f);
        FadeOut();
    }

    public float getFadeTime() {
        return time;
    }

    public void FadeIn() {
        if(tempCor!=null) {
            StopCoroutine(tempCor);
        }
        tempCor = StartCoroutine(FadeTo(1f));
    }

    public void FadeOut() {
        if(tempCor!=null) {
            StopCoroutine(tempCor);
        }
        tempCor = StartCoroutine(FadeTo(0f));
    }

    public void SetFadePanelAlpha(float fadeAlpha) {
        tempColor = fadeImg.color;
        tempColor.a = fadeAlpha;
        fadeImg.color = tempColor;
    }

    IEnumerator FadeTo(float fadeAlpha) {
        lerpValue = 0f;
        float currentAlpha = fadeImg.color.a;
        tempColor = fadeImg.color;
        while(lerpValue < time) {
            tempColor.a = Mathf.Lerp(currentAlpha,fadeAlpha,(lerpValue/time));
            fadeImg.color = tempColor;
            lerpValue += Time.deltaTime;
            yield return null;
        }
        tempColor.a = fadeAlpha;
        fadeImg.color = tempColor;
    }
}