﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ClickObject : Interactable
{
    public List<CustomEvent> events = new List<CustomEvent>();

    [System.Serializable]
    public class CustomEvent {
        public float delay;
        public UnityEvent onClick;
    }

    public override void Click() {
        StartCoroutine(PlayEvents());
    }
    
    IEnumerator PlayEvents() {
        for (int i = 0; i < events.Count; i++)
        {
            yield return new WaitForSeconds(events[i].delay);
            events[i].onClick.Invoke();
        }
    }
}