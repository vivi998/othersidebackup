﻿using System.Collections;
using UnityEngine;

public class ThrowObject : Interactable
{
    public float throwSpeed= 14f;

    Vector3 startPos;
    Quaternion startRot;
    Vector3 startScale;

    Vector3 gravity = new Vector3(0f,-1200f,0f);
    Vector3 fixVelocity;
    Material dissolveMat;
    Rigidbody rb;
    Collider col;

    float rotationSpeed = 2f;
    float defaultMass;

    bool waitForPlayerToGetOut;
    bool draggingObj;
    bool canDrag=true;

    private void Start() {
        rb = GetComponent<Rigidbody>();
        dissolveMat = GetComponent<MeshRenderer>().materials[0];
        col = GetComponent<Collider>();
        defaultMass = rb.mass;
        startPos = transform.position;
        startRot = transform.rotation;
        startScale = transform.localScale;
    }

    public override void Press() {
        PickUpObject();
    }

    public override void Release() {
        DropObject();
    }

    public override void Drag() {
        DragObject();
    }

    private void PickUpObject() {
        if(canDrag) {
            CancelInvoke(nameof(isPlayerFarFromObject));
            rb.mass = 0.01f;
            waitForPlayerToGetOut = false;
            gameObject.layer = 9;
            draggingObj = true;
        }
    }

    private void DropObject() {
        draggingObj = false;
        rb.mass = defaultMass;
        
        fixVelocity = rb.velocity;
        fixVelocity.x = Mathf.Clamp(rb.velocity.x,-throwSpeed,throwSpeed);
        fixVelocity.y = Mathf.Clamp(rb.velocity.y,-throwSpeed,throwSpeed);
        fixVelocity.z = Mathf.Clamp(rb.velocity.z,-throwSpeed,throwSpeed);
        rb.velocity = fixVelocity;

        rb.AddTorque(new Vector3(rb.velocity.z,0f,-rb.velocity.x)*rotationSpeed,ForceMode.Impulse);

        if(!PlayerMovement.instance.isGroudned()) {
            gameObject.layer = 9;
            CancelInvoke(nameof(isPlayerFarFromObject));
            Invoke(nameof(isPlayerFarFromObject),0.5f);
        }
        else {
            isPlayerFarFromObject();
        }
    }

    private void DragObject() {
        if(draggingObj) {
            Vector3 fakeVelocity = ((PlayerMovement.instance.grabObject.transform.position - transform.position) * 20);
            rb.velocity = fakeVelocity;
        }
    }

    private void isPlayerFarFromObject() {
        if(Vector3.Distance(transform.position,PlayerMovement.instance.transform.position) > 1.5f) {
            gameObject.layer = 13;
            waitForPlayerToGetOut = false;
        }
        else {
            gameObject.layer = 9;   
            waitForPlayerToGetOut = true;
        }
    }

    private void Update() {
        if(waitForPlayerToGetOut) {
            isPlayerFarFromObject();
        }

        if(!draggingObj) {
            rb.AddForce(gravity*rb.mass*Time.deltaTime);
        }

        if(Input.GetKeyDown(KeyCode.J)) {
            DissolveAndReset();
        }
    }

    public void DissolveAndReset() {
        StartCoroutine(DissolveEffect());
    }

    float lerpValue;
    float time=0.5f;
    string dissolveProperty="_dissolve";

    IEnumerator DissolveEffect() {
        canDrag = false;
        col.enabled = false;
        DropObject();
        rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
        
        lerpValue = 0f;
        while(lerpValue < time) {
            dissolveMat.SetFloat(dissolveProperty,lerpValue/time);
            lerpValue += Time.deltaTime;
            yield return null;
        }

        dissolveMat.SetFloat(dissolveProperty,1);
        rb.velocity=Vector3.zero;
        rb.angularVelocity = Vector3.zero; 
        transform.position = startPos;
        transform.rotation = startRot;
        transform.localScale = startScale;

        lerpValue = 0f;
        while(lerpValue < time) {
            dissolveMat.SetFloat(dissolveProperty,(1 - lerpValue/time));
            lerpValue += Time.deltaTime;
            yield return null;
        }


        dissolveMat.SetFloat(dissolveProperty,-1);
        rb.constraints = RigidbodyConstraints.None;
        isPlayerFarFromObject();
        col.enabled = true;
        canDrag = true;
    }
}