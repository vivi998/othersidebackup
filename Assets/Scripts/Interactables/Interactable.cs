﻿using System.Reflection;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public string interactableName;

    public virtual void Press() {
    }

    public virtual void Release() {
    }

    public virtual void Click() {
    }

    public virtual void Drag() {
    }
}