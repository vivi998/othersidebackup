﻿using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class MultyTriggerObject : MonoBehaviour
{
    public List<TriggerEvent> triggerEvents = new List<TriggerEvent>();
    int eventId;

    private void OnTriggerEnter(Collider other) {
        if(other.GetComponent<Interactable>()!=null) {
            eventId = GetTriggerEvent(other.GetComponent<Interactable>().interactableName);
            if(eventId>=0) {
                triggerEvents[eventId].onTriggerEnter.Invoke();
            }
        }
        else if(other.GetComponent<PlayerMovement>()!=null) {
            eventId = GetTriggerEvent("player");
            if(eventId>=0) {
                triggerEvents[eventId].onTriggerEnter.Invoke();
            }
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.GetComponent<Interactable>()!=null) {
            eventId = GetTriggerEvent(other.GetComponent<Interactable>().interactableName);
            if(eventId>=0) {
                triggerEvents[eventId].onTriggerExit.Invoke();
            }
        }
        else if(other.GetComponent<PlayerMovement>()!=null) {
            eventId = GetTriggerEvent("player");
            if(eventId>=0) {
                triggerEvents[eventId].onTriggerExit.Invoke();
            }
        }
    }

    private int GetTriggerEvent(string otherName) {
        for (int i = 0; i < triggerEvents.Count; i++)
        {
            if(triggerEvents[i].name.Equals(otherName)) {
                return i;
            }
        }
        return -1;
    }
}

[System.Serializable]
public class TriggerEvent {
    public string name;
    public UnityEvent onTriggerEnter,onTriggerExit;
}