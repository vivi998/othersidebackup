﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

[System.Serializable]
public class SingleCrosshair{
    public GameObject crosshairObj;
    public float maxFade = 1f;
}

public class Crosshair : MonoBehaviour
{
    public SingleCrosshair[] crosshairs;

    Coroutine tempCor;
    Color currentColor,newColor;
    
    float lerpValue=0f;
    float time=0.17f;
    float tempAlpha;
    int lastIndex;
    int currentIndex;

    bool fadingIn,fadeingOut;

    private void Awake() {
        lastIndex = 0;
    }
    
    public void SetCrosshair(int index) {
        if(currentIndex == index) {
            return;
        }
        if(tempCor!=null) {
            StopCoroutine(tempCor);
        }
        crosshairs[lastIndex].crosshairObj.SetActive(false);
        lastIndex = currentIndex;
        currentIndex = index;
        tempCor = StartCoroutine(FadeCrosshairs(currentIndex));
    }

    IEnumerator FadeCrosshairs(int index) {
        lerpValue = 0f;
        
        currentColor = crosshairs[lastIndex].crosshairObj.GetComponent<Image>().color;
        tempAlpha = currentColor.a;

        newColor = crosshairs[index].crosshairObj.GetComponent<Image>().color;
        newColor.a = 0f;
        crosshairs[index].crosshairObj.GetComponent<Image>().color = newColor;
        crosshairs[index].crosshairObj.SetActive(true);

        while(lerpValue < time) {
            currentColor.a = Mathf.Lerp(tempAlpha,0f,lerpValue/time);
            newColor.a = Mathf.Lerp(0f,crosshairs[index].maxFade,lerpValue/time);
            crosshairs[lastIndex].crosshairObj.GetComponent<Image>().color = currentColor;
            crosshairs[index].crosshairObj.GetComponent<Image>().color = newColor;
            lerpValue += Time.deltaTime;
            yield return null;
        }

        currentColor.a = 0f;
        newColor.a = crosshairs[index].maxFade;
        crosshairs[lastIndex].crosshairObj.GetComponent<Image>().color = currentColor;
        crosshairs[index].crosshairObj.GetComponent<Image>().color = newColor;
        crosshairs[lastIndex].crosshairObj.SetActive(false);
        lastIndex = index;
    }
}