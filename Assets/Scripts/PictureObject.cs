﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PictureObject : MonoBehaviour
{
    public Transform[] extraPoints;
    public UnityEvent onToggleOn,onToggleOff;
    
    Renderer rend;
    RaycastHit hit;
    Animator animator;

    bool toggle;

    private void Start() {
        rend = GetComponent<Renderer>();
        animator = GetComponent<Animator>();

        PlayerMovement.instance.onTakePicture.AddListener(PictureTaken);
    }

    private void OnDisable() {
        PlayerMovement.instance.onTakePicture.RemoveListener(PictureTaken);
    }

    void PictureTaken() {
        if(isVisible()) {
            if(toggle) {
                onToggleOn.Invoke();
            }
            else {
                onToggleOff.Invoke();
            }
            toggle = !toggle;
        }
    }

    bool isVisible() {
        for (int i = 0; i < extraPoints.Length; i++)
        {
            if(shootRaycastToPlayer(extraPoints[i].position)) {
                return true;
            }
        }
        return shootRaycastToPlayer(transform.position);
    }

    bool shootRaycastToPlayer(Vector3 pos) {
        Physics.Raycast(pos, (PlayerMovement.instance.cameraObject.transform.position - pos), out hit, Mathf.Infinity, PlayerMovement.instance.cameraLayerMask);
        if(hit.collider.gameObject.Equals(PlayerMovement.instance.gameObject) && rend.IsVisibleFrom(PlayerMovement.instance.cameraObject)) {
            return true;
        }
        return false;
    }
}