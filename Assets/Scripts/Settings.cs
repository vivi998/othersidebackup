﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    public Dropdown resolutionDropdown;
    public List<MyResolution> allResolutions = new List<MyResolution>();
    int value;

    void Start()
    {
        Resolution[] resolutions = Screen.resolutions;

        foreach (var res in resolutions)
        {
            resolutionDropdown.options.Add(new Dropdown.OptionData(res.width + "x" + res.height + " " + res.refreshRate+"Hz"));
            MyResolution newRes = new MyResolution(res.width,res.height,res.refreshRate);
            allResolutions.Add(newRes);
        }

        SetResolution(allResolutions.Count-1);
    }

    public void SetResolution(int index = -1) {
        value = index >= 0 ? index : resolutionDropdown.value;
        Screen.SetResolution(allResolutions[value].myWidth, allResolutions[value].myheight, true, allResolutions[value].myrefreshRate);
    }
}

[System.Serializable]
public class MyResolution {
    public MyResolution() {}
    public MyResolution(int width, int height,int refreshRate) {
        myWidth = width;
        myheight = height;
        myrefreshRate = refreshRate;
    }
    public int myWidth;
    public int myheight;
    public int myrefreshRate;
}
