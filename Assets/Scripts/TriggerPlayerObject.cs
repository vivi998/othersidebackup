﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine.Events;
using UnityEngine;

public class TriggerPlayerObject : MonoBehaviour
{
    public bool onlyOnce=true;
    public List<CustomEvent> events = new List<CustomEvent>();
    Coroutine tempCor;

    bool enteredOnce,exitedOnce;

    [System.Serializable]
    public class CustomEvent {
        public float delay;
        public UnityEvent onEnter,onExit;
    }

    private void OnTriggerEnter(Collider other) {
        if(other.GetComponent<PlayerMovement>()!=null) {
            if(enteredOnce) {
                return;
            }
            if(onlyOnce) {
                enteredOnce = true;
            }
            if(tempCor!=null) {
                StopCoroutine(tempCor);
            }
            StartCoroutine(PlayEvents(true));
        }
    }

    // private void OnTriggerStay(Collider other) {
    //     if(other.GetComponent<PlayerMovement>()!=null) {
    //         if(enteredOnce) {
    //             return;
    //         }
    //         if(onlyOnce) {
    //             enteredOnce = true;
    //         }
    //         if(tempCor!=null) {
    //             StopCoroutine(tempCor);
    //         }
    //         StartCoroutine(PlayEvents(true));
    //     }
    // }

    private void OnTriggerExit(Collider other) {
        if(other.GetComponent<PlayerMovement>()!=null) {
            if(exitedOnce) {
                return;
            }
            if(onlyOnce) {
                exitedOnce = true;
            }
            if(tempCor!=null) {
                StopCoroutine(tempCor);
            }
            StartCoroutine(PlayEvents(false));
        }
    }
    
    IEnumerator PlayEvents(bool enter) {
        for (int i = 0; i < events.Count; i++)
        {
            yield return new WaitForSeconds(events[i].delay);
            if(enter) {
                events[i].onEnter.Invoke();
            }
            else {
                events[i].onExit.Invoke();
            }
        }
    }
}
