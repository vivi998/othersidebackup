﻿using UnityEngine;
using UnityEngine.UI;

public class FPS : MonoBehaviour
{
    float updateInterval = 0.2F;
    float accum = 0;
    float timeleft;

    int frames = 0;

    public int fpsCap = 300;

    Text fpsText;

    void Start()
    {
        fpsText = GetComponent<Text>();
        timeleft = updateInterval;
        Application.targetFrameRate = fpsCap;
        QualitySettings.vSyncCount = 0;
        QualitySettings.antiAliasing = 8;
    }

    void Update()
    {
        timeleft -= Time.unscaledDeltaTime;
        accum += 1 / Time.unscaledDeltaTime;
        ++frames;

        if (timeleft <= 0.0)
        {
            float fps = accum / frames;
            string format = System.String.Format("{0:F2} FPS", fps);
            timeleft = updateInterval;
            accum = 0.0F;
            frames = 0;
            fpsText.text = fps.ToString("0");
        }
    }
}
