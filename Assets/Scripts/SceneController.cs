﻿using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine;

public class SceneController : Controllers
{
    public static SceneController instance;

    Coroutine tempCor;

    private void Awake() {
        instance = this;
    }

    public void NextScene() {
        int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
        if (SceneManager.sceneCount < nextSceneIndex)
        {
            nextSceneIndex = 0;
        }

        if(tempCor!=null) {
            return;
        }
        tempCor = StartCoroutine(LoadSceneAsync(nextSceneIndex));
    }

    public void ReloadScene() {
        if(tempCor!=null) {
            return;
        }
        tempCor = StartCoroutine(LoadSceneAsync(SceneManager.GetActiveScene().buildIndex));
    }

    public void GoToScene(int index) {
        if(tempCor!=null) {
            return;
        }
        tempCor = StartCoroutine(LoadSceneAsync(index));
    }

    IEnumerator LoadSceneAsync(int index)
    {
        UIController.instance.fadePanel.FadeIn();
        yield return new WaitForSeconds(UIController.instance.fadePanel.getFadeTime());
        AsyncOperation loader = SceneManager.LoadSceneAsync("LoadingScene",LoadSceneMode.Single);
        while (!loader.isDone)
        {
            yield return null;
        }
        // Debug.Log("Loading screen loaded trying to load " + index);

        yield return new WaitForSeconds(0.3f);
        UIController.instance.fadePanel.SetFadePanelAlpha(1f);

        if(index>0) {
            UIController.instance.crosshair.gameObject.SetActive(true);
            UIController.instance.mainMenu.gameObject.SetActive(false);
        }
        else {
            UIController.instance.crosshair.gameObject.SetActive(false);
            UIController.instance.mainMenu.gameObject.SetActive(true);
        }

        AsyncOperation asyncLevel = SceneManager.LoadSceneAsync(index,LoadSceneMode.Single);
        while (!asyncLevel.isDone)
        {
            yield return null;
        }
        yield return new WaitForSeconds(0.3f);
        UIController.instance.fadePanel.FadeOut();
        tempCor = null;
    }
}