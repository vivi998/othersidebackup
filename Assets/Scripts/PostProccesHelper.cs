﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostProccesHelper : MonoBehaviour
{
    public void SetCameraView() {
        PostProccesController.instance.SetView(true);
        PlayerMovement.instance.SetView(true);
    }

    public void SetNormalView() {
        PostProccesController.instance.SetView(false);
        PlayerMovement.instance.SetView(false);
    }
}
