﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

public class PostProccesController : MonoBehaviour
{
    public static PostProccesController instance;

    Volume postProccesVolume;

    LensDistortion lensDistortion;
    Vignette vignette;

    private void Awake() {
        instance = this;
        postProccesVolume = GetComponent<Volume>();
        postProccesVolume.profile.TryGet(out lensDistortion);
        postProccesVolume.profile.TryGet(out vignette);
        SetView(false);
    }

    public void SetView(bool b) {
        lensDistortion.active = b;
        vignette.active = b;
    }

    public bool isCameraViewActive() {
        return lensDistortion.active;
    }

    public void TakePicture() {
        StartCoroutine(vignetteFadeAnimation(0.1f));
    }

    float vignetteIntensity;
    float elapsedTime;

    IEnumerator vignetteFadeAnimation(float time) {
        vignette.intensity.max = 1;
        vignetteIntensity = vignette.intensity.value;
        elapsedTime = 0f;
        while (elapsedTime < time)
        {
            vignette.intensity.value = Mathf.Lerp(vignetteIntensity,vignette.intensity.max,elapsedTime / time);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        elapsedTime = 0f;
        while (elapsedTime < time)
        {
            vignette.intensity.value = Mathf.Lerp(vignette.intensity.max,0.35f,elapsedTime / time);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }
}
