﻿using UnityEngine;

public class Bicycle : Interactable
{
    #region Values

    [HideInInspector]
    public WheelCollider frontWheel,backWheel;
    [HideInInspector]
    public Transform frontWheelTransform;
    [HideInInspector]
    public Transform backWheelTransform;
    [HideInInspector]
    public Transform SteeringWheel;
    [HideInInspector]
    public Transform frontWheelHolder;
    [HideInInspector]
    public Transform seatPosition;

    public float maxSteerAngle = 28f;
    public float motorForce = 1200f;

    PlayerMovement playerMovement;
    Rigidbody rigidbody;
    Quaternion _quat;

    Vector3 centerOfMass = new Vector3(0f,0f,0.2f);
    Vector3 steeringWheelAngle;
    Vector3 slowDownVelocity;
    Vector3 _pos;

    float horizontal;
    float vertical;
    float steeringAngle;
    bool onBike;

#endregion
    
    private void Start() {
        rigidbody = GetComponent<Rigidbody>();
        playerMovement = PlayerMovement.instance;
        rigidbody.centerOfMass = centerOfMass;
    }

    public override void Click() {
        Mount();
        // Move it to some button maybe and on click drag
    }

    void Mount() {
        PlayerMovement.blockMovement = true;
        playerMovement.transform.position = seatPosition.position;
        playerMovement.transform.parent = transform;
        playerMovement.SetCameraClamp(40f,40f,120f,120f);
        frontWheel.brakeTorque = 0f;
        backWheel.brakeTorque = 0f;
        onBike = true;
    }

    void Dismount() {
        playerMovement.transform.parent = null;
        playerMovement.transform.position = seatPosition.transform.position - seatPosition.transform.right;
        PlayerMovement.blockMovement = false;
        playerMovement.SetCameraClamp();
        onBike = false;
    }

    void Update() {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x,transform.eulerAngles.y,0f);
        if(onBike) {
            if(Input.GetKeyDown(KeyCode.LeftShift)) {
                Dismount();
            }
        }
        else if(rigidbody.velocity.z > 0.1f || rigidbody.velocity.x > 0.1f) {
            slowDownVelocity = rigidbody.velocity;
            slowDownVelocity = Vector3.MoveTowards(slowDownVelocity,Vector3.zero,16f*Time.deltaTime);
            slowDownVelocity.y = rigidbody.velocity.y;
            rigidbody.velocity = slowDownVelocity;
            UpdateWheelPose(frontWheel,frontWheelTransform);
            UpdateWheelPose(backWheel,backWheelTransform);
        }
        else {
            frontWheel.brakeTorque = 5f;
            backWheel.brakeTorque = 5f;
            rigidbody.angularVelocity = Vector3.zero;
        }
    }

    void FixedUpdate() {
        if(onBike) {
            HandleBikeMovement();
        }
    }

    void HandleBikeMovement() {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        steeringAngle = (maxSteerAngle-rigidbody.velocity.magnitude) * horizontal;
        frontWheel.steerAngle = steeringAngle;

        if(vertical>=0f) {
            rigidbody.velocity = Vector3.ClampMagnitude(rigidbody.velocity, 26f); 
        }
        else {
            rigidbody.velocity = Vector3.ClampMagnitude(rigidbody.velocity, 8f);
        }

        if(Input.GetKey(KeyCode.Space)) {
            rigidbody.velocity = Vector3.MoveTowards(rigidbody.velocity,Vector3.zero, 20f * Time.deltaTime);
        }

        frontWheel.motorTorque = vertical * motorForce;
        backWheel.motorTorque = vertical * motorForce;

        if(rigidbody.velocity.magnitude > 0.1f) {
            UpdateWheelPose(frontWheel,frontWheelTransform);
            UpdateWheelPose(backWheel,backWheelTransform);
        }
        else {
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
        }

        SteeringWheel.localEulerAngles = new Vector3(0f,steeringAngle+horizontal*10f,0f);
    }

    void UpdateWheelPose(WheelCollider _collider,Transform _transform) {
        _pos = _transform.position;
        _quat = _transform.rotation;

        _collider.GetWorldPose(out _pos, out _quat);

        if(_transform==frontWheelTransform) {
            _collider.transform.position = frontWheelHolder.position;
            steeringWheelAngle = _quat.eulerAngles;
            steeringWheelAngle.y += horizontal*10f;
            _transform.rotation = Quaternion.Euler(steeringWheelAngle);
        }
        else {
            _transform.rotation = _quat;
            _transform.position = _pos;
        }

    }
}