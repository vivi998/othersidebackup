﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class PlayerMovement : MonoBehaviour 
{
    public static PlayerMovement instance;

    #region Values
    
    [Header("Values")]
    public float walkSpeed = 200f;
    public float runSpeed = 350f;
    public float crouchSpeed = 100f;
    public float jumpSpeed = 400f;
    public float gravity = 22f;
    public float sensitivity = 0.7f;
    public float fovSpeed = 25f;
    [Header("Layer masks")]
    public LayerMask crouchLayerMask;
    public LayerMask cameraLayerMask;
    public LayerMask interactableLayerMask;

    [Header("Helper Objects")]
    public Animator cameraAnimation;
    public GameObject grabObject;
    public Camera cameraObject;

    public static bool blockMovement=false;

    [HideInInspector]
    public UnityEvent onTakePicture;

    CharacterController characterController;
    Transform currentLadder;
    Collider[] hitColliders;
    GameObject grabbedObj;
    GameObject rayObject;

    Vector3 direction;
    Vector3 movement;
    Vector3 flatMovement;
    Vector3 moveDirection;
    Vector3 eulerRotation;
    Vector3 jumpedForce;
    Vector3 hitNormal;
    Vector3 normalCameraPos = new Vector3(0f,0.6f,0f);
    Vector3 crouchCameraPos = new Vector3(0f,0.2f,0f);
    Vector3 jumpOffLadderForce = new Vector3(-5f,0f,0f);
    Vector3 eularAngles;
    Vector3 xRotateVector;

    float speed = 0f;
    float horizontal = 0f;
    float vertical = 0f;
    float mouseX = 0f;
    float mouseY = 0f;
    float currentY = 0f;
    float pushPower = 2f;
    float cameraCrouchMoveSpeed=3f;
    float crouchOverlapSphereRadius = 0.5f;
    float yClampMin = 85f;
    float yClampMax = 85f;
    float xClampMin = 181f;
    float xClampMax = 181f;
    float fieldOfView = 60f;
    float maxFov = 64f;
    float minFov = 60f;

    bool onLadder;
    bool remainCrouched;
    bool isGrounded;
    bool isNormalView;
    
    #endregion

    void Awake() {
        instance = this;
        characterController = GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
        isNormalView = false;
        CheckView();
        currentY = -cameraObject.transform.eulerAngles.x;
    }

    void Update()
    {
        eularAngles = transform.localEulerAngles;
        eularAngles.z = 0f;
        eularAngles.x = 0f;
        transform.localEulerAngles = eularAngles;
        HandleFov();
        HandleToggleView();
        HandleCameraRotation(yClampMin,yClampMax,xClampMin,xClampMax);
        if(!blockMovement) {
            HandleMovement();
            HandleRayCast();
        }
    }

    void HandleMovement() {
        horizontal = blockMovement ? 0f : Input.GetAxis("Horizontal");
        vertical = blockMovement ? 0f : Input.GetAxis("Vertical");

        direction = new Vector3(horizontal,0,vertical);

        if(onLadder && currentLadder && !blockMovement) {
            speed = walkSpeed;
            movement = currentLadder.InverseTransformDirection(direction);
            flatMovement = speed * Time.fixedDeltaTime * transform.TransformDirection(movement);
            jumpedForce = Vector3.zero;
            direction = new Vector3(horizontal,0,0);
            Vector3 newX = transform.TransformDirection(direction) * speed * Time.fixedDeltaTime;
            moveDirection = new Vector3(newX.x,flatMovement.x,newX.z);

            characterController.Move(moveDirection*Time.deltaTime);
            
            // Jump from ladder
            if(Input.GetKeyDown(KeyCode.Space)) {
                moveDirection = Vector3.zero;
                jumpedForce = currentLadder.TransformDirection(jumpOffLadderForce);
                onLadder = false;
                currentLadder = null;
            }

            if(isGroudned()) {
                onLadder = false;
                currentLadder = null;
            }
        }
        else {

            movement = transform.TransformDirection(direction);

            if(!blockMovement) {
                
                if(Input.GetKeyUp(KeyCode.LeftControl)) {
                    remainCrouched = CanStandUp();
                }
                else if(remainCrouched) {
                    speed = crouchSpeed;
                    remainCrouched = CanStandUp();
                }
                else if(Input.GetKey(KeyCode.LeftControl)) {
                    speed = crouchSpeed;
                }
                else if(Input.GetKey(KeyCode.LeftShift)) {
                    speed = runSpeed;
                    fieldOfView = maxFov;
                }
                else {
                    speed = walkSpeed;
                    fieldOfView = minFov;
                }
            }

            if(speed==crouchSpeed) {
                SetHeight(1.2f);
                if(cameraObject.transform.localPosition!=crouchCameraPos) {
                    cameraObject.transform.localPosition = Vector3.MoveTowards(cameraObject.transform.localPosition,crouchCameraPos,cameraCrouchMoveSpeed*Time.deltaTime);
                }
            }
            else {
                SetHeight(1.98f);
                if(cameraObject.transform.localPosition!=normalCameraPos) {
                    cameraObject.transform.localPosition = Vector3.MoveTowards(cameraObject.transform.localPosition,normalCameraPos,cameraCrouchMoveSpeed*Time.deltaTime);
                }
            }
                
            flatMovement = speed * Time.fixedDeltaTime * movement;

            moveDirection = new Vector3(flatMovement.x,moveDirection.y,flatMovement.z);

            // Jump
            if(isGrounded && isGroudned() && Input.GetKeyDown(KeyCode.Space) && !blockMovement) {
                moveDirection.y = jumpSpeed * Time.fixedDeltaTime;
                jumpedForce = flatMovement;
            }
            else if(isGroudned()) {
                if(jumpedForce!=Vector3.zero) {
                    jumpedForce = Vector3.zero;
                }
                moveDirection.y = -7f;
            }
            else {
                moveDirection.y -= (gravity*Time.deltaTime);
            }

            // Strafe
            if(!isGroudned() && jumpedForce!=Vector3.zero) {
                Vector3 strafeVector = new Vector3(jumpedForce.x+(moveDirection.x*2f),0f,jumpedForce.z+(moveDirection.z*2f));
                strafeVector = Vector3.ClampMagnitude(strafeVector,jumpedForce.magnitude);
                strafeVector.y = jumpedForce.y+moveDirection.y;
                characterController.Move(strafeVector*Time.deltaTime);
            }
            else {
                if (!isGrounded) {
                    moveDirection.x += (1f - hitNormal.y) * hitNormal.x * (5f - 0.3f);
                    moveDirection.z += (1f - hitNormal.y) * hitNormal.z * (5f - 0.3f);
                }

                characterController.Move(moveDirection*Time.deltaTime);
                
                isGrounded = (Vector3.Angle (Vector3.up, hitNormal) <= 45f);
            }
        }
    }
  
    void HandleRayCast() {
        if(Input.GetMouseButtonDown(0)) {
            rayObject = ShootRay();
            if(rayObject && rayObject.GetComponent<Interactable>()!=null) {
                grabbedObj = rayObject.gameObject;
                rayObject.GetComponent<Interactable>().Press();
            }
        }
        else if(grabbedObj && grabbedObj.GetComponent<Interactable>()!=null) {
			grabbedObj.GetComponent<Interactable>().Drag();
            UIController.instance.crosshair.SetCrosshair(1);
			if((Input.GetMouseButtonUp(0) && grabbedObj!=null)||(grabbedObj!=null && Vector3.Distance(grabbedObj.transform.position,transform.position)>4f)) {
				grabbedObj.GetComponent<Interactable>().Release();
                rayObject = ShootRay();
                if(rayObject && rayObject.GetComponent<Interactable>()!=null && rayObject == grabbedObj) {
                    rayObject.GetComponent<Interactable>().Click();
                }
				grabbedObj = null;
			}
		}
        else {
            rayObject = ShootRay();
            if(rayObject && rayObject.GetComponent<Interactable>()!=null) {
                //Looking at the object
                if(UIController.instance!=null) {
                    UIController.instance.crosshair.SetCrosshair(2);
                }
            }
            else {
                if(UIController.instance!=null) {
                    UIController.instance.crosshair.SetCrosshair(0);
                }
            }
        }
    }

    GameObject ShootRay() {
        RaycastHit hit;
        if (Physics.Raycast(cameraObject.transform.position, cameraObject.transform.TransformDirection(Vector3.forward), out hit, 2.75f,interactableLayerMask))
        {
			return hit.collider.gameObject;
        }
        else {
            return null;
        }
    }

    void HandleCameraRotation(float yClampMin=90f,float yClampMax=90f,float xClampMin=181f,float xClampMax=181f)
	{
        mouseX = Input.GetAxis("Mouse X") * sensitivity;
        mouseY = Input.GetAxis("Mouse Y") * sensitivity;

		currentY += mouseY;

		if (currentY > yClampMax)
		{
			currentY = yClampMax;
			mouseY = 0.0f;
			ClampXAxisRotationToValue(360-yClampMax);
		}
		else if (currentY < -yClampMin)
		{
			currentY = -yClampMin;
			mouseY = 0.0f;
			ClampXAxisRotationToValue(yClampMin);
		}

		cameraObject.transform.Rotate(Vector3.left * mouseY);
		transform.Rotate(Vector3.up * mouseX);

        xRotateVector = transform.localRotation.ToEulerAngles()* Mathf.Rad2Deg;
        xRotateVector.y = Mathf.Clamp(xRotateVector.y,-xClampMin,xClampMax);
        transform.localEulerAngles = xRotateVector;
    }

    void HandleFov() {
        if(cameraObject.fieldOfView!=fieldOfView) {
            cameraObject.fieldOfView = Mathf.MoveTowards(cameraObject.fieldOfView,fieldOfView,fovSpeed*Time.deltaTime);
        }
    }

    public void SetView(bool b) {
        isNormalView = b;
        CheckView();
    }
    
    void HandleToggleView() {
        if(Input.GetMouseButtonDown(1)) {
            cameraAnimation.SetBool("cameraOn",true);
        }
        else if(Input.GetMouseButtonUp(1)) {
            cameraAnimation.SetBool("cameraOn",false);
        }

        if(PostProccesController.instance.isCameraViewActive() && Input.GetMouseButtonUp(0) && grabbedObj == null) {
            onTakePicture.Invoke();
            PostProccesController.instance.TakePicture();
        }
    }

    void CheckView() {
        Physics.IgnoreLayerCollision (10, 11, !isNormalView);
        Physics.IgnoreLayerCollision (10, 12, isNormalView);
        Physics.IgnoreLayerCollision (9, 11, !isNormalView);
        Physics.IgnoreLayerCollision (9, 12, isNormalView);
        Physics.IgnoreLayerCollision (13, 11, !isNormalView);
        Physics.IgnoreLayerCollision (13, 12, isNormalView);

        if(isNormalView) {
            interactableLayerMask = 1 | 2048 | 8192;
            cameraLayerMask = 1 | 2 | 4 | 16 | 32 | 256 | 512 | 2048 | 8192;
        }
        else {
            interactableLayerMask = 1 | 4096 | 8192;
            cameraLayerMask = 1 | 2 | 4 | 16 | 32 | 256 | 512 | 4096 | 8192;
        }
        cameraObject.cullingMask = cameraLayerMask;
    }

    void OnTriggerEnter(Collider other) {
        if(other.tag=="Ladder") {
            onLadder = true;
            currentLadder = other.transform;
        }
    }

    void OnTriggerExit(Collider other) {
        if(other.tag=="Ladder") {
            onLadder = false;
            currentLadder = null;
        }
    }

    bool CanStandUp() {
        hitColliders = Physics.OverlapSphere(cameraObject.transform.position+crouchCameraPos, crouchOverlapSphereRadius,crouchLayerMask);
        return hitColliders.Length != 0 ? true : false;
    }

    void OnControllerColliderHit (ControllerColliderHit hit) {
        Debug.Log(hit.collider.name);
        // hitNormal = hit.normal; // Slope
        // Rigidbody body = hit.collider.attachedRigidbody;
        // if (body == null || body.isKinematic)
        // {
        //     return;
        // }
        // if (hit.moveDirection.y < -0.3)
        // {   
        //     Vector3 pushDira = new Vector3(hit.moveDirection.x * pushPower, body.velocity.y, hit.moveDirection.z * pushPower);
        //     body.velocity = pushDira;
        //     return;
        // }
        // Vector3 pushDir = new Vector3(hit.moveDirection.x * pushPower, body.velocity.y, hit.moveDirection.z * pushPower);
        // body.velocity = pushDir;
    }

    public bool isGroudned() {
        if(characterController!=null) {
            return characterController.isGrounded;
        }
        else {
            return false;
        }
    }

    void SetHeight(float index) {
        if(characterController.height != index) {
            characterController.height = index;
            if(index>=2f) {
                characterController.center = Vector3.zero;
            }
            else {
                Vector3 newCenter = characterController.center;
                newCenter.y = -0.25f;
                characterController.center = newCenter;
            }
        }
    }

    public void SetCameraClamp(float yMin=90f,float yMax=90f,float xMin=181f,float xMax=181f) {
        yClampMin = yMin;
        yClampMax = yMax;
        xClampMin = xMin;
        xClampMax = xMax;
    }

    void ClampXAxisRotationToValue(float value) {
		eulerRotation = transform.localEulerAngles;
		eulerRotation.x = value;
        eulerRotation.z = 0f;
        eulerRotation.y = 0f;
		cameraObject.transform.localEulerAngles = eulerRotation;
	}
}