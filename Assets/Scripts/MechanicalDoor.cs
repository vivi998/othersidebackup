﻿using UnityEngine;

public class MechanicalDoor : MonoBehaviour
{
    Animator animator;

    public bool openOnStart=false;

    private void Start() {
        animator = GetComponent<Animator>();
        if(openOnStart) {
            Open();
        }
    }

    public void Open() {
        Debug.Log("Open!");
        animator.SetBool("opened",true);
    }
    
    public void Close() {
        Debug.Log("Close!");
        animator.SetBool("opened",false);
    }
}