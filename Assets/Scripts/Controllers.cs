﻿using System.Collections.Generic;
using UnityEngine;

public class Controllers : MonoBehaviour
{
    public static Controllers mainController;

    public List<Controllers> allControllers = new List<Controllers>();

    private void Awake() {
        mainController = this;
        DontDestroyOnLoad(this);
    }
}
