﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructor : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) {
        if(other.GetComponent<ThrowObject>()!=null) {
            other.GetComponent<ThrowObject>().DissolveAndReset();
        }
    }

    // private void OnTriggerExit(Collider other) {
    //     if(other.GetComponent<ThrowObject>()!=null) {
    //         other.GetComponent<ThrowObject>().DissolveAndReset();
    //     }
    // }
}