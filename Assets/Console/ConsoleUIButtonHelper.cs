﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ConsoleUIButtonHelper : MonoBehaviour , IDragHandler , IPointerClickHandler , IPointerDownHandler , IPointerUpHandler , IPointerEnterHandler , IPointerExitHandler
{
    public UnityEvent onClick;
    public UnityEvent onDrag;
    public UnityEvent onPress;
    public UnityEvent onRelease;
    public UnityEvent onEnter;
    public UnityEvent onExit;

    public bool draggable;

    public void OnDrag(PointerEventData eventData)
    {
        if(draggable) {
            Console.instance.consoleHolder.GetComponent<RectTransform>().anchoredPosition += eventData.delta / Console.instance.transform.parent.GetComponent<Canvas>().scaleFactor;
            Vector2 newRect = Console.instance.consoleHolder.GetComponent<RectTransform>().anchoredPosition;
            newRect.x = Mathf.Clamp(newRect.x,-700f,700f);
            newRect.y = Mathf.Clamp(newRect.y,-700f,45f);
            Console.instance.consoleHolder.GetComponent<RectTransform>().anchoredPosition = newRect;
        }
        onDrag.Invoke();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        onClick.Invoke();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        onPress.Invoke();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        onEnter.Invoke();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        onExit.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        onRelease.Invoke();
        Console.instance.FocusInput();
    }
}
