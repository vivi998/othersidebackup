﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;

public class Console : MonoBehaviour
{
    public static Console instance;

    public InputField inputField;
    public ScrollRect scrollRect;

    public GameObject consoleHolder;

    public GameObject textPrefab;
    public GameObject contentObj;

    public Text[] suggestionsTextObjects;

    public List<String> suggestions = new List<String>();

    int counter;


    Text selectedSuggestedText;
    int suggestTextSelectID=-1;

    private void Awake() {
        if(instance) {
            Destroy(gameObject);
        }
        else {
            instance = this;
            DontDestroyOnLoad(transform.parent.gameObject);
        }
        consoleHolder.SetActive(false);
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.BackQuote)) {
            ToggleConsole();
        }

        if((Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)) && consoleHolder.activeSelf) {
            if(selectedSuggestedText) {
                SuggestionClicked(selectedSuggestedText);
                if(!inputField.text.Contains(" ")) {
                    Submit();
                }
            }
            else {
                Submit();
            }
        }

        if(Input.GetKeyDown(KeyCode.DownArrow)) {
            Select(1);
        }
        else if(Input.GetKeyDown(KeyCode.UpArrow)) {
            Select(-1);
        }
    }

    public void Submit() {
        CheckCommand(inputField.text);
        ClearInputText();
        FocusInput();
        SearchSuggestions();
    }

    public void OnValueChanged() {
        SearchSuggestions();
        ResetSelect();
        Select(1);
    }

    void ResetSelect() {
        selectedSuggestedText = null;
        suggestTextSelectID = -1;
    }

    void Select(int index) {
        if(counter==0) {
            return;
        }
        if(selectedSuggestedText) {
            selectedSuggestedText.transform.GetChild(0).gameObject.SetActive(false);
        }
        suggestTextSelectID+=index;
        if(suggestTextSelectID>=counter) {
            suggestTextSelectID = 0;
        }
        else if(suggestTextSelectID < 0) {
            suggestTextSelectID = counter-1;
        }
        selectedSuggestedText = suggestionsTextObjects[suggestTextSelectID];
        selectedSuggestedText.transform.GetChild(0).gameObject.SetActive(true);
    }


    void SearchSuggestions() {
        counter=0;

        if(inputField.text=="") {
            for (int i = counter; i < suggestionsTextObjects.Length; i++)
            {
                suggestionsTextObjects[i].gameObject.SetActive(false);
            }
            return;
        }

        for (int i = 0; i < suggestions.Count; i++)
        {
            if(suggestions[i].ToLower().Contains(inputField.text.ToLower()) && suggestions[i]!=inputField.text) {
                suggestionsTextObjects[counter].text = suggestions[i];
                suggestionsTextObjects[counter].gameObject.SetActive(true);
                suggestionsTextObjects[counter].transform.GetChild(0).gameObject.SetActive(false);
                counter++;
                if(counter >= suggestionsTextObjects.Length) {
                    return;
                }
            }
        }

        for (int i = counter; i < suggestionsTextObjects.Length; i++)
        {
            suggestionsTextObjects[i].gameObject.SetActive(false);
        }
    }

    public void SuggestionClicked(Text textObj) {
        inputField.text = textObj.text;
        FocusInput();
        ResetSelect();
    }

    public void ToggleConsole() {
        PlayerMovement.blockMovement = !PlayerMovement.blockMovement;
        consoleHolder.SetActive(PlayerMovement.blockMovement);
        Cursor.lockState = !PlayerMovement.blockMovement ? CursorLockMode.Locked : CursorLockMode.None;
        if(PlayerMovement.blockMovement) {
            FocusInput();
        }
        ClearInputText();
    }

    void ClearInputText() {
        inputField.text = "";
    }

    public void FocusInput() {
        inputField.ActivateInputField();
        StartCoroutine(FocusInputCor());
    }

    IEnumerator FocusInputCor() {
        yield return new WaitForEndOfFrame();
        inputField.caretPosition = inputField.text.Length;
        inputField.ForceLabelUpdate();
    }

    void CheckCommand(string command) {
        if(CheckIfCommandWithValue(command)) {
            return;
        }
        
        command = command.Trim(' ');
        
        switch(command) {
            case "clear":
                ClearConsole();
            break;
            case "fullscreen":
            #if UNITY_EDITOR
                EditorWindow window = EditorWindow.focusedWindow;
                window.maximized = !window.maximized;
            #endif
            break;
            case "restart":
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                ConsoleLog("] "+ command);
                ToggleConsole();
            break;
            case "pause":
            #if UNITY_EDITOR
                EditorApplication.isPaused = true;
            #endif
            break;
            case "quit":
            #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
            #endif
                Application.Quit();
            break;
            case "":
                ConsoleLog("] "+ command);
            break;
            case "close":
                ToggleConsole();
            break;
            default:
                ConsoleLog("Unknown command: "+ command);
            break;
        }
    }

    bool CheckIfCommandWithValue(string command) {
        try
        {
            string[] stringArray = command.Split(' ');
            if(stringArray.Length>2) {
                return false;
            }

            if(stringArray.Length==2) {
                float value = float.Parse(stringArray[1]);

                switch(stringArray[0]) {
                    case "walkSpeed":
                    PlayerMovement.instance.walkSpeed = value;
                    ConsoleLog("Walk speed set to = " + value);
                    break;
                    case "runSpeed":
                    PlayerMovement.instance.runSpeed = value;
                    ConsoleLog("Run speed set to = " + value);
                    break;
                    case "crouchSpeed":
                    PlayerMovement.instance.crouchSpeed = value;
                    ConsoleLog("Crouch speed set to = " + value);
                    break;
                    case "jumpSpeed":
                    PlayerMovement.instance.jumpSpeed = value;
                    ConsoleLog("Jump speed set to = " + value);
                    break;
                    case "gravity":
                    PlayerMovement.instance.gravity = value;
                    ConsoleLog("Gravity set to = " + value);
                    break;
                    case "sensitivity":
                    PlayerMovement.instance.sensitivity = value;
                    ConsoleLog("Sensitivity set to = " + value);
                    break;
                    default:
                    return false;
                }
            }
            else {
                return false;
            }

            return true;
        }
        catch (System.Exception)
        {
            Debug.Log("Molim?");
            return false;
            throw;
        }
    }

    void ConsoleLog(string text) {
        GameObject textObj = Instantiate(textPrefab,transform.position,Quaternion.identity,contentObj.transform);
        textObj.GetComponent<Text>().text = text;
        StartCoroutine(ForceScrollDown());
    }

    IEnumerator ForceScrollDown () {
        yield return new WaitForEndOfFrame ();
        scrollRect.verticalNormalizedPosition = 0f;
    }

    void ClearConsole() {
        for (int i = contentObj.transform.childCount; i > 0; i--)
        {
            Destroy(contentObj.transform.GetChild(i-1).gameObject);
        }
    }

    public void Log(string text) {
        Debug.Log(text);
        ConsoleLog(text);
    }
}
